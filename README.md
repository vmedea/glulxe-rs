glulxe-rs
=========

This crate provides a binding from Rust to the Glulxe interpreter for interactive fiction,
an implementation of the glulx specification. The crate embeds the glulx interpreter.

For this to work, the binary using the crate needs to provide handlers for the
Glk functions that the interpreter will invoke, see the `glk` crate for this.

- [Glulx VM specification](https://www.eblong.com/zarf/glulx/)

## How to use

The crate provides two functions:

- To initialize and configure the interpreter, call `glulxe::init(gamefile)` where gamefile is
  an open Glk stream that contains the story file to run.

- To start the interpreter, call `glulxe::main()`.

From there on, Glulxe takes over execution on that thread. It will call `glk_exit()` when
execution is done.

## To do

- Currently the `VM_DEBUGGER` is not supported. It might be offered as a crate feature.
  This would need a dependency on the C library `libxml`.

## Example: toyglk

The `glk` crate has an example of using this crate to implement a glulx story
file interpreter.

## Alternatives

- There is the [glulx](https://github.com/thefarwind/glulx-rs) crate that implements
  a Glulx interpreter in native rust, however it is not currently (as of end 2019)
  far enough along to run story files.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

(note that the inner crate, `glulxe-sys` is licensed under a MIT license only,
because it is based on files from glulxe which are licensed such)

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
