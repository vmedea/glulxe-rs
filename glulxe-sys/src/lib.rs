#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
use glk_sys::{frefid_t, schanid_t, strid_t, winid_t, event_t, glkdate_t, glktimeval_t, stream_result_t};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
