extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    let mut base_config = cc::Build::new();

    // Define OS specific preprocessor directive for osdepend.c
    let target = env::var("TARGET").expect("TARGET was not set");
    if target.contains("apple-darwin") || target.contains("apple-ios") {
        base_config.define("OS_MAC", "1");
    } else if target.contains("windows") {
        // Windows needs no defines: WIN32 is defined automatically.
    } else {
        // For all other targets, hope that UNIX works.
        base_config.define("OS_UNIX", "1");
    }

    base_config
        .include("glk/")
        .file("glulxe/accel.c")
        .file("glulxe/debugger.c")
        .file("glulxe/exec.c")
        .file("glulxe/files.c")
        .file("glulxe/float.c")
        .file("glulxe/funcs.c")
        .file("glulxe/gestalt.c")
        .file("glulxe/glkop.c")
        .file("glulxe/heap.c")
        .file("glulxe/main.c")
        .file("glulxe/operand.c")
        .file("glulxe/osdepend.c")
        .file("glulxe/profile.c")
        .file("glulxe/search.c")
        .file("glulxe/serial.c")
        .file("glulxe/string.c")
        .file("glulxe/vm.c")
        // Disable known GCC warnings that crop up in glulx interpreter
        .flag_if_supported("-Wno-sign-compare")
        .flag_if_supported("-Wno-unused-parameter")
        .flag_if_supported("-Wno-unused-value")
        .flag_if_supported("-Wno-unused-function")
        .flag_if_supported("-Wno-implicit-fallthrough")
        .compile("libglulxe.a");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .clang_arg("-Iglk")
        // Blacklist glk types that need to come from the glk crate.
        .blacklist_type("winid_t")
        .blacklist_type("strid_t")
        .blacklist_type("frefid_t")
        .blacklist_type("schanid_t")
        .blacklist_type("event_t")
        .blacklist_type("glkdate_t")
        .blacklist_type("glktimeval_t")
        .blacklist_type("stream_result_t")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
