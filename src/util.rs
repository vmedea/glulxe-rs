use glk_sys::strid_t;
use std::os::raw::c_char;

use glulxe_sys;

/** Simple heuristic to figure out if binary file is a blorb file or a ulx file,
 * @returns None if neither, or when an error occured.
 */
pub fn is_blorb(gamefile: strid_t) -> Result<bool, ()> {
    let mut buf = [0u8; 12];
    unsafe {
        glulxe_sys::glk_stream_set_position(gamefile, 0, glulxe_sys::seekmode_Start);
    }
    if unsafe { glulxe_sys::glk_get_buffer_stream(gamefile, buf.as_mut_ptr() as *mut c_char, 12) }
        != 12
    {
        // Short read, this can't be a blorb file nor a ulx file
        return Err(());
    }
    if buf[0] == b'G' && buf[1] == b'l' && buf[2] == b'u' && buf[3] == b'l' {
        Ok(false)
    } else if buf[0] == b'F'
        && buf[1] == b'O'
        && buf[2] == b'R'
        && buf[3] == b'M'
        && buf[8] == b'I'
        && buf[9] == b'F'
        && buf[10] == b'R'
        && buf[11] == b'S'
    {
        Ok(true)
    } else {
        Err(())
    }
}
