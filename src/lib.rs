/*! Provide bindings for the Glulxe interpreter.
 */
use glk_sys::{self, strid_t};

use glulxe_sys;

mod util;

/** Initialize Glulx interpreter (set game file stream).
 */
pub fn init(gamefile: strid_t) -> Result<(), ()> {
    let is_blorb = util::is_blorb(gamefile)?;
    unsafe {
        /* For full flexibility, we bypass the OS-specific initialization that Glulx does and
         * provide settings (currently only the game file, this may be extended in the future to
         * debug and profiling settings) directly.
         */
        glulxe_sys::gamefile = gamefile;
        glulxe_sys::locate_gamefile(is_blorb as i32);
    }
    Ok(())
}

/** Run Glulx interpreter main loop.
 */
pub fn main() {
    glk_sys::main();
}
